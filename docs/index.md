# MouseTools

[![PyPI version](https://badge.fury.io/py/MouseTools.svg)](https://badge.fury.io/py/MouseTools) [![Downloads](https://pepy.tech/badge/mousetools)](https://pepy.tech/project/mousetools)


An unofficial Python wrapper for the Disney API. Data is pulled directly from Disney. This package supports Walt Disney World and Disneyland.

### Installation
You can install using pip:
```bash
pip install MouseTools
```
You can also install directly from this repo in case of any changes not uploaded to Pypi.
```bash
pip install git+https://gitlab.com/caratozzoloxyz/public/MouseTools
```

### License
This project is distributed under the MIT license. For more information see [LICENSE](https://gitlab.com/caratozzoloxyz/public/MouseTools/-/blob/master/LICENSE?ref_type=heads)

### Disclaimer
This project is in no way affiliated with The Walt Disney Company and all use of Disney Services is subject to the [Disney Terms of Use](https://disneytermsofuse.com/).

This package uses the [ThemeParks.wiki API](https://themeparks.wiki/). 