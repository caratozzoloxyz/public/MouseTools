# Entity Types

These subclasses extend [Entity][mousetools.entities.base.EntityBase] to provide differentiation between types.

::: mousetools.entities
    options:
        show_root_heading: true