# Usage

The Disney API considers everything an entity. Attractions, resorts, restaurants, etc, are all entities.

## Getting Started

You'll want to create an Enitity object by using the enitity's id.

```python
from mousetools.entities import Attraction

big_thunder = Attraction("80010110")

```

You can also use the extended id.
```python
from mousetools.entities import Attraction

big_thunder = Attraction("80010110;entityType=attraction")

```


All entities inherit from [EntityBase][mousetools.entities.base.EntityBase]. 

```python
print(big_thunder.name)
#> 'Big Thunder Mountain Railroad'

print(big_thunder.ancestor_theme_park_id)
#> '80007944'

print(big_thunder.get_status())
#> 'OPERATING'

print(big_thunder.get_wait_time())
#> 45

```

!!! note

    All entities are lazy loaded, meaning there is no request sent to load the data until an object's attributes are accessed. This allows objects to be created faster, and reduce requests to Disney's servers until the data is actually needed.


For ease of use, there are two Destination objects already created. You can use the different [Entity Types][mousetools.ids.FacilityServiceEntityTypes] to get children entities.

```python
from mousetools.entities import WALT_DISNEY_WORLD_DESTINATION, DISNEYLAND_RESORT_DESTINATION
from mousetools.ids import FacilityServiceEntityTypes

print(WALT_DISNEY_WORLD_DESTINATION.get_children_entity_ids(FacilityServiceEntityTypes.ENTERTAINMENT_VENUES))
#> ['10460', '80008033', '80008259']

print(DISNEYLAND_RESORT_DESTINATION.get_children_entity_ids(FacilityServiceEntityTypes.RESORT_AREAS))
#> ['330338', '15597760']
```
