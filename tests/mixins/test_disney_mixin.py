import httpx
import pytest

from mousetools.ids import DestinationIds, FacilityServiceEntityTypes
from mousetools.mixins.disney import DisneyAPIMixin
from mousetools.urls import (
    WDPRAPPS_FACILITY_SERVICE_BASE_URL,
    WDPRO_FACILITY_SERVICE_BASE_URL,
)

URLS = []
URLS.append(
    f"{WDPRO_FACILITY_SERVICE_BASE_URL}/{FacilityServiceEntityTypes.DESTINATIONS}/{DestinationIds.WALT_DISNEY_WORLD}/{FacilityServiceEntityTypes.ATTRACTIONS}"
)
URLS.append(
    f"{WDPRO_FACILITY_SERVICE_BASE_URL}/{FacilityServiceEntityTypes.DESTINATIONS}/{DestinationIds.WALT_DISNEY_WORLD}/{FacilityServiceEntityTypes.RESORTS}"
)
URLS.append(
    f"{WDPRO_FACILITY_SERVICE_BASE_URL}/{FacilityServiceEntityTypes.DESTINATIONS}/{DestinationIds.WALT_DISNEY_WORLD}/{FacilityServiceEntityTypes.THEME_PARKS}"
)
# TODO add URLS for each entity id


@pytest.fixture
def disney_mixin():
    return DisneyAPIMixin()


@pytest.mark.parametrize("url", URLS)
def test_disney_mixin(disney_mixin, url):
    resp = disney_mixin.get_disney_data(url)

    assert isinstance(resp, dict)


@pytest.mark.parametrize(
    "url",
    [
        f"{WDPRO_FACILITY_SERVICE_BASE_URL}",
        f"{WDPRAPPS_FACILITY_SERVICE_BASE_URL}",
        f"{WDPRO_FACILITY_SERVICE_BASE_URL}/{FacilityServiceEntityTypes.DESTINATIONS}/123",
    ],
)
def test_nonexistent_url(disney_mixin, url):
    with pytest.raises(httpx.HTTPError):
        disney_mixin.get_disney_data(url)
